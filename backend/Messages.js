const express = require('express');
const cors = require('cors');
const app = express();
const port = 8000;
const secret = 'secretCode';

const Vigenere = require('caesar-salad').Vigenere;
app.use(express.json());
app.use(cors());

app.post('/encode', (req, res) => {
    console.log(req.body);
    const encodedMessage = Vigenere.Cipher(req.body.password).crypt(req.body.message);
    console.log(encodedMessage, 'encodedMessage');
    res.send(encodedMessage);
});


app.post('/decode', (req, res) => {
    const decodedMessage = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    res.send(`Decoded message: ${decodedMessage}`);
});

app.listen(port, () => {
    console.log(` Hello ${port}`);

});