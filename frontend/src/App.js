import React, { Component } from 'react';
import './App.css';
import axios from 'axios';

class App extends Component {
    state = {
    code: {
        encoded: '',
        decoded: '',
        password: ''
    }
    };

    postEncode = () => {
        if (this.state.code.password) {
            axios.post('http://localhost:8000/encode', {
                message: this.state.code.decoded,
                password: this.state.code.password
            }).then(response => {
                console.log(response.data);
                this.setState({code: {...this.state.code, encoded: response.data}})

            })
        } else {
            alert('You have to write password')
        }

    };

    postDecode = () => {
        if(this.state.code.password) {
            axios.post('http://localhost:8000/decode', {
                message: this.state.code.encoded,
                password: this.state.code.password
            }).then(response => {
                // console.log(response.data);
                this.setState({code: {...this.state.code, decoded: response.data}})
            })
        } else {
            alert('You have to write password')
        }
    };

    codeValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                code: {...prevState.code, [e.target.name]: e.target.value}
            }
        });
    };

  render() {
      console.log(this.state.code);
    return (
      <div className="Messages">
          <textarea name="decoded" value={this.state.code.decoded} placeholder="Decoded Message" onChange={this.codeValueChanged}>
          </textarea>
          <input type="text" name="password" value={this.state.code.password} onChange={this.codeValueChanged} placeholder="Password"/>
          <button onClick={this.postEncode}>Encode</button>
          <button onClick={this.postDecode}>Decode</button>
          <textarea name="encoded" value={this.state.code.encoded} onChange={this.codeValueChanged} placeholder="Encoded Message">
          </textarea>
      </div>
    );
  }
}

export default App;
